package br.com.filipe.desafio.respository;

import br.com.filipe.desafio.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepositoy extends JpaRepository<Cliente, Long> {

    @Query("SELECT c FROM Cliente c WHERE (:cpf is null or c.cpf = :cpf) and (:nome is null or c.nome = :nome)")
    Page<Cliente> findAllByCpfAndNome(String cpf, String nome, Pageable paginacao);

}
