package br.com.filipe.desafio.controller;

import br.com.filipe.desafio.model.Cliente;
import br.com.filipe.desafio.respository.ClienteRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path="/clientes")
public class ClienteController {

    @Autowired
    ClienteRepositoy clienteRepository;

    @GetMapping
    public ResponseEntity<Page<Cliente>> findAll(@PageableDefault(page = 0, size = 10, sort = "id", direction = Direction.DESC) Pageable paginacao,
                                                 @RequestParam(value = "cpf", required = false) String cpf,
                                                 @RequestParam(value = "nome", required = false) String nome) {
        return ResponseEntity.ok(clienteRepository.findAllByCpfAndNome(cpf, nome, paginacao));
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<Cliente> findAll(@PathVariable("id") Long id) {
        if (isClienteExistente(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(clienteRepository.findById(id).get());
    }

    @PostMapping
    public ResponseEntity<Cliente> create(@RequestBody Cliente cliente) {
        return ResponseEntity.ok(clienteRepository.save(cliente));
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<Cliente> update(@PathVariable("id") Long id, @RequestBody Cliente cliente) {
        if (isClienteExistente(id)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(clienteRepository.save(cliente));
    }

    private boolean isClienteExistente(@PathVariable("id") Long id) {
        Optional<Cliente> clienteConsulta = clienteRepository.findById(id);
        if (!clienteConsulta.isPresent()) {
            return true;
        }
        return false;
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<?>  delete(@PathVariable("id") Long id){
        if (isClienteExistente(id)) {
            return ResponseEntity.notFound().build();
        }
        clienteRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(value="/{id}")
    public ResponseEntity<?> patchUpdate(@PathVariable("id") Long id, @RequestBody Cliente patchCliente) {
        Optional<Cliente> clienteConsulta = clienteRepository.findById(id);
        if (!clienteConsulta.isPresent()){
            return ResponseEntity.notFound().build();
        }

        Cliente cliente = clienteConsulta.get();

        if (patchCliente.getNome() != null){
            cliente.setNome(patchCliente.getNome());
        }
        if (patchCliente.getCpf() != null){
            cliente.setCpf(patchCliente.getCpf());
        }
        if (patchCliente.getDataNascimento() != null){
            cliente.setDataNascimento(patchCliente.getDataNascimento());
        }

        clienteRepository.save(cliente);
        return ResponseEntity.ok().build();
    }

}
