CREATE TABLE IF NOT EXISTS public.cliente (
	id bigserial NOT NULL,
	cpf varchar(11) NOT NULL UNIQUE,
	data_nascimento timestamp NULL,
	nome varchar(255) NOT NULL,
	CONSTRAINT cliente_pkey PRIMARY KEY (id)
);

insert into public.cliente(nome, cpf, data_nascimento) values ('Filipe Lins', '02323993526', '1991-08-07');
